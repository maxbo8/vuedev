const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Документация по Vue.js',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'Введение',
        link: '/guide/',
      },
      {
        text: 'Разное',
        link: '/other/'
      },
      {
        text: 'Официальная документация по Vue.js',
        link: 'https://ru.vuejs.org/index.html'
      }
    ],
    sidebar: {
      '/guide/': [
        {
          title: 'Введение',
          collapsable: false,
          children: [
            'Введение',
          ]
        },
        {
          title: 'Основы Vue.js',
          collapsable: false,
          children: [
            'Основы Vue',
            'Компоненты'
          ]
        },
        {
          title: 'Vuex',
          collapsable: false,
          children: [
            'Vuex'
          ]
        },
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
