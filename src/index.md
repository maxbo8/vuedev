---
home: true
heroImage: images/logo.svg
tagline:
actionText: Перейти к документации 🚀
actionLink: /guide/
features:
- title: Основы Vue.js
  details: Feature 1 Description
- title: Vuex
  details: Feature 2 Description
- title: Vue Router
  details: Feature 3 Description
footer: Made by Max with ❤️
---
